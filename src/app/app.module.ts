import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './Shared/components/header/header.component';
import { SidebarComponent } from './Shared/components/sidebar/sidebar.component';
import { FooterComponent } from './Shared/components/footer/footer.component';

import { DefaultComponent } from './_Layouts/default/default.component';
import { HomeComponent } from './_Views/home/home.component';
import { PostComponent } from './_Views/post/post.component';
import { LoginComponent } from "./_Views/login/login.component";

import { FlexLayoutModule } from "@angular/flex-layout";
import { MaterialModule } from "src/app/material-module";
import { UserdetailsComponent } from './_Views/userdetails/userdetails.component';
import { ProjectaddComponent } from './_Views/projectadd/projectadd.component';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AuthGuard } from './guards/auth.guard'; 
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxPaginationModule } from 'ngx-pagination';
//import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ToastrModule } from 'ngx-toastr';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    DefaultComponent,
    HomeComponent,
    PostComponent,
    UserdetailsComponent,
    LoginComponent,
    ProjectaddComponent,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    Ng2SearchPipeModule,
    NgSelectModule,NgxPaginationModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot(),
    NgMultiSelectDropDownModule,
    NgbModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
