import { Component, OnInit, ChangeDetectorRef, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { ProjectDetailsDTO } from 'src/app/_Models/project-details-dto';
import { EmployeeDTO } from 'src/app/_Models/employee-dto';
import { PortfolioDTO } from 'src/app/_Models/portfolio-dto';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CompanyDTO } from 'src/app/_Models/company-dto';
import { LoginDTO } from 'src/app/_Models/login-dto';
import { LoginComponent } from '../login/login.component';
import { ProjecttypeDTO } from 'src/app/_Models/projecttype-dto';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';
import { NotificationService } from 'src/app/_Services/notification.service';
import { Shareportfolio_DTO } from 'src/app/_Models/shareportfolio';
import * as _ from 'underscore';
import { from, Subscription } from 'rxjs';
import { of } from 'rxjs';
import { distinct } from 'rxjs/operators';
//import { map } from 'rxjs/operators';
import { TileStyler } from '@angular/material/grid-list/tile-styler';
import { Data, Router } from '@angular/router';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from 'src/app/app.module';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ElementRef } from '@angular/core';
import { CdkRow } from '@angular/cdk/table';
// platformBrowserDynamic().bootstrapModule
// (AppModule).then(ref=>{
//   if(window['ngRef']){
//     window['ngRef'].destroy();
//   }
//   window['ngRef']=ref;
// }).catch(err=>console.error(err));

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  dropdownList = [];
  ProtypeArrlist: any = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = {};
  dropdownSettings_Emp: IDropdownSettings = {};
  dropdownSettings_Cmp: IDropdownSettings = {};
  dropdownSettings_Stat: IDropdownSettings = {};
  dropdownSettings_Team: IDropdownSettings = {};
  _SelectedEmployees=[];

  ObjUserDetails: UserDetailsDTO;
  _obj: PortfolioDTO;
  ProjectTypelist: ProjecttypeDTO[];
  Search_Value: any;
  ProjectDetails_List: ProjectDetailsDTO[];
  _ProjectDataList: any = [];
  // subscription: Subscription;
  // PPro_Subscription: Subscription;
  Obj_Portfolio_DTO: PortfolioDTO;
  objLoginDTO: LoginDTO;
  EmployeeList: EmployeeDTO[];
  Company_List: CompanyDTO[];
  searchText: string;
  PortfolioSearch: string;
  search_Type: string;
  search_status: string;
  search_Team: string;
  checkbox: boolean;
  Selected_project_List = [];
  obj = {};
  Portfolio_FormGroup: FormGroup;
  selectedRowIds: Set<number> = new Set<number>();
  //===Booleans
  submitted: boolean;
  PortfolioList: boolean;
  matCard: boolean;
  _ProjectList: boolean;
  _PortfolioListTable: boolean;
  master_checked: boolean = false;
  btn_CreatePortfolio: boolean;
  // btnAddPortfolio: string = "Add Portfolio";
  Custom_chkbxvalue: boolean;
  matprogressbar: boolean;
  p: number = 1; //Paging
  objLoginComp: LoginComponent;
  objUserDetails: UserDetailsDTO;
  Current_user: string;
  UserDetailList: UserDetailsDTO[];
  ProTypeselectedId: any;
  Spinnerloading = false;
  //===================//
  EmpNo: string; EmpCompNo: string; SystemRole: string
  findedDataByType: any;
  SearchByType_List = [];
  //===================//
  progressbarValue = 0;
  curSec: number = 0;
  isLoading = true;
  _PortfolioList: PortfolioDTO[];
  CompanyDropdown: string;
  EmployeeDropdown: string;
  ngCompanyDropdown: any;
  ngEmployeeDropdown: Map<string,Array<any>>;
  ngEmployeeDropdown2: any=[];
  PortfolioNames_List: PortfolioDTO[];
  PortfolioNested_list: [];
  txtPortName: boolean;
  _ExistingPortfolioOfProjects = [];
  _ProjectsBasedonPortfolio: any;
  _ExistingPortfolioAddedProjectsList: any;
  _SharedPortfolios: any;
  Current_user_ID: any;
  _ProjectsListBy_Pid: PortfolioDTO[];
  With_Data: PortfolioDTO[];
  _PrferencesData: PortfolioDTO[];
  Teamlist = [];
  StatusList = [];

  _CompanyNo: string;
  _EmployeeId: string;
  _Preferences: string;
  _PortFolio_Namecardheader: string;
  _Pid: any;
  Share_preferences: boolean;
  preferences: any;
  _ShareDetailsList: any;
  ObjSharePortfolio: Shareportfolio_DTO;
  _PreferencesList: any;
  selectedItemsList = [];
  checkedIDs = [];
  _PreferenceType: string;
  _ErrorMessage_comp: string;
  _ErrorMessage_User: string;
  _ErrorMessage_Pref: string;
  count: number = 1;
  btnPrevious: boolean;
  btn_Share = true;
  btn_addProject: boolean;
  PreferenceTpye: number
  btnGetRecords: boolean;
  //Filter Projects
  ProjectTyp_value: string;
  Status_value: string;
  Emp_value: string;
  returnUrl = '/Dashboard';
  modal: any;


  constructor(public service: ProjectTypeService,
    private objFormBuilder: FormBuilder,
    private notifyService: NotificationService,
    private cdr: ChangeDetectorRef, private router: Router,
  ) {
    this.ObjUserDetails = new UserDetailsDTO;
    this.Obj_Portfolio_DTO = new PortfolioDTO();
    this.ObjSharePortfolio = new Shareportfolio_DTO();
    this.Portfolio_FormGroup = this.objFormBuilder.group({
      'portfolioName': new FormControl('', [Validators.required])
    });
  }
  ngOnInit() {
   
    this.btnGetRecords = true;
    this.PortfolioList = false;
    this._PortfolioListTable = true;
    this._ProjectList = true;
    this.Share_preferences = true;
    this.btn_CreatePortfolio = true;
    this.Current_user = localStorage.getItem('_Currentuser');
    this.Current_user_ID = sessionStorage.getItem('EmpNo');
    this.searchText = '';
    this.search_Team = '0';
    this.search_Type = '0';
    this.search_status = '0';
    this.service.ProjectDetails_List = [];
    this.selectedItemsList = [];
    this.checkedIDs = [];
    console.log("Login By :", sessionStorage.getItem('EmpNo'));
    this.GetPortfolioByEmployee();
    this.GetProjectsByUserName();
   // this.GetProjectTypeDropDownList();
    this.GetPortfolioByEmployee();
  }
  OnProjtypeChange(ptype) {
    //debugger
    this.ProjectTyp_value = ptype['Exec_BlockName'];
    if (!this.ProjectTyp_value) {
      this.ProjectTyp_value = "";
    }
  }
  OnTeamChange(emp) {
    this.Emp_value = emp['TM_DisplayName'];
    this.btnGetRecords = false;
  }
  EmpDeselect() {
    this.Emp_value = "";
  }
  StatDeselect() {
    this.Status_value = "";
  }
  ProTypeDeselect() {
    this.ProjectTyp_value = "";
  }
  OnStatusChange(stus) {
    this.Status_value = stus['Status'];
    if (!this.Status_value) {
      this.Status_value = "";
    }
  }
  Btn_GetRecords() {
    this.GetProjectsByUserName();
  }
  GetProjectsByUserName() {
    debugger
    this.ProjectDetails_List = [];
    this.ObjUserDetails.PageNumber = this.count;
    this.service.GetProjectsByUserName(this.count, this.ProjectTyp_value, this.Emp_value, this.Status_value, this.searchText).subscribe(data => {
      this._ProjectDataList = data;
      //this.ProjectTypelist = Array.
      this.Teamlist = Array.from(this._ProjectDataList.reduce((m, t) => m.set(t.TM_DisplayName, t), new Map()).values());
      this.StatusList = Array.from(this._ProjectDataList.reduce((m, t) => m.set(t.Status, t), new Map()).values());
      this.dropdownList = Array.from(this._ProjectDataList.reduce((m, t) => m.set(t.Exec_BlockName, t), new Map()).values());
      this.dropdownSettings = {
        singleSelection: true,
        idField: 'Project_Block',
        textField: 'Exec_BlockName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
      };
      this.Teamlist = this.Teamlist.sort((a, b) => (a.TM_DisplayName > b.TM_DisplayName) ? 1 : -1);
      console.log("Team--->", this.Teamlist)
      this.dropdownSettings_Team = {
        singleSelection: true,
        idField: 'id',
        textField: 'TM_DisplayName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
      };
      this.StatusList = this.StatusList.sort((a, b) => (a.Status > b.Status) ? 1 : -1);
      console.log("status--->", this.StatusList)
      this.dropdownSettings_Stat = {
        singleSelection: true,
        idField: 'id',
        textField: 'Status',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
      };
    });
  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  trackByid(index: number, item: any) {
    debugger
    return item.Project_Code;
  }
  get f() {
    return this.Portfolio_FormGroup.controls;
  }
  Reset() {
    this.searchText = '';
  }
  Displayprojectlist() {
    this.Portfolio_FormGroup.controls['portfolioName'].enable();
    this.Selected_project_List = [];
    this.btn_CreatePortfolio = true;
    this.PortfolioList = true;
    this._PortfolioListTable = true;
    this._ProjectList = false;
    this.Obj_Portfolio_DTO.Portfolio_ID = null;
    this.Portfolio_FormGroup.reset();
  }
  CloseBttn() {
    this.p = 1;
    this.Share_preferences = true;
    this.GetPortfolioByEmployee();
    this._ProjectList = true;
    this.PortfolioList = false;
    this.searchText = '';
    this.search_Team = '0';
    this.search_Type = '0';
    this.search_status = '0';
    this.master_CheckBox();
    this._PreferencesList = [];
  }
  //Master Check Box check/Un-checkAll
  master_CheckBox() {
    for (let value of Object.values(this._ProjectDataList)) {
      value['checked'] = this.master_checked;
      if (value['checked'] === true) {
        this.btn_CreatePortfolio = false;
      }
      else {
        this.btn_CreatePortfolio = true;
      }
    }
  }
  checkboxclick() {
    debugger
    for (let value of Object.values(this._ProjectDataList)) {
      if (value['checked'] === true) {
        this.btn_CreatePortfolio = false;
        return true;
      }
      else {
        this.btn_CreatePortfolio = true;
      }
    }
  }
  fetchSelectedItems() {
    this.selectedItemsList = this._ProjectDataList.filter((checkboxes) => {
      return checkboxes.checked == true;
    });
  }
  dataTableCheckbox() {
    this.btnGetRecords = true;
    this.fetchSelectedItems();
  }
  // ----Selected Rows Starts-
  // Selected Rows Ends----------------
  onSearchProjects(event: any) {
    this.Search_Value = event.target.value;
    if (this.Search_Value === "") {
      this.matCard = true;
    }
    else {
      this.matCard = false;
    }
  }
  GetProjectDetails() {
    this.service.GetProjects_EPDB().subscribe(res =>
      this.service.ProjectDetails_List = res as ProjectDetailsDTO[]);
  }
  OnCardClick(P_id: number, P_Name: string) {
    debugger
    this._ShareDetailsList = [];
    this._PortfolioListTable = false;
    this.PortfolioList = true;
    this.Obj_Portfolio_DTO.Portfolio_Name = P_Name;
    this.Obj_Portfolio_DTO.Portfolio_ID = P_id;
    this._Pid = P_id;
    sessionStorage.setItem('Pid', this._Pid);
    this._PortFolio_Namecardheader = P_Name;
    this.Obj_Portfolio_DTO.Portfolio_Name = P_Name;
    //Get Projects
    this._ProjectsListBy_Pid = [];
    this.service.GetProjectsBy_portfolioId(P_id)
      .subscribe((data) => {
        console.log("qwerty" + data);
        debugger
        this._ProjectsListBy_Pid = JSON.parse(data[0]['JosnProjectsByPid']);
        this._ShareDetailsList = JSON.parse(data[0]['SharedDetailsJson']);
        this.PreferenceTpye = data[0]["PreferenceType"];

        this.With_Data = JSON.parse(data[0]['EmployeePreferenceJson']);
        this.Share_preferences = false;
        if (this.PreferenceTpye == 1) {
          if (this.With_Data[0].Preferences == "View Only") {
            this.Share_preferences = true;
          }
          else if (this.With_Data[0].Preferences == "Full Access") {
            this.Share_preferences = false;
          }
        }
        else if (this.PreferenceTpye == 0) {
          this.Share_preferences = false;
        }
        console.log("_ProjectsListBy_Pid", this._ProjectsListBy_Pid);
      });
    this.cdr.detectChanges();
  }
  populateForm(Obj_P: number) {
    debugger
    this.Selected_project_List = [];
    this.PortfolioList = true;
    this._PortfolioListTable = true;
    this._ProjectList = false;
    this.btn_CreatePortfolio = true;
    this.Portfolio_FormGroup.controls['portfolioName'].disable();
   // this.Obj_Portfolio_DTO.Portfolio_Name = JSON.parse(Obj_P.Portfolio_Name);
    this.Obj_Portfolio_DTO.Portfolio_ID = Obj_P;
    //this.Obj_Portfolio_DTO.;
    let arr1: any = this._ProjectsListBy_Pid;
    let arr2: any = this._ProjectDataList;
    var selectedIds = _.map(arr1, (d) => { return d.Project_Code })
    this._ProjectDataList = _.reject(arr2, (d) => {
      var findId = _.find(selectedIds, (sId) => { return sId === d.Project_Code });
      if (findId) {
        return true;
      }
      else {
        return false;
      }
    })
  }
  OnSave() {
    debugger
    this.submitted = true;
    if (this.Portfolio_FormGroup.invalid) {
      return this.notifyService.showWarning("Portfolio Name Required !!", 'Please Enter Portfolio Name');
    }
    if (this.Obj_Portfolio_DTO.Portfolio_ID == null) {
      this.Obj_Portfolio_DTO.Portfolio_Name = this.Portfolio_FormGroup.get('portfolioName').value;
      this.Obj_Portfolio_DTO.Portfolio_ID = 0;
      this.Obj_Portfolio_DTO.SelectedProjects = this.selectedItemsList;
      this.Obj_Portfolio_DTO.Status = this.selectedItemsList[0]['Status'];
      let LengthOfSelectedItems = JSON.stringify(this.selectedItemsList.length);
      this.service.SavePortfolio(this.Obj_Portfolio_DTO)
        .subscribe(data => {
          // this.getDocTypeData();
          this.service.GetPortfolioByEmployee()
            .subscribe(
              (data) => {
                // alert(JSON.stringify(data));
                this._PortfolioList = data as PortfolioDTO[];
                this._ExistingPortfolioOfProjects = this._PortfolioList;
                console.log("Portfolio:", this._ExistingPortfolioOfProjects);
                this.cdr.detectChanges();
              });
        });
      this.notifyService.showInfo("Successfully" + ' ' + 'Added' + ' ' + LengthOfSelectedItems + ' ' + 'Project', ' New Portfolio Created');
    }
    else {
      debugger
      this.Obj_Portfolio_DTO.Portfolio_Name = this.Portfolio_FormGroup.get('portfolioName').value;
      this.Obj_Portfolio_DTO.Portfolio_ID;
      this.Obj_Portfolio_DTO.SelectedProjects = this.selectedItemsList;
      this.Obj_Portfolio_DTO.Status = this.selectedItemsList[0]['Status'];
      let LengthOfSelectedItems = JSON.stringify(this.selectedItemsList.length);
      this.service.SavePortfolio(this.Obj_Portfolio_DTO)
        .subscribe(data => {
          // this.getDocTypeData();
          this.service.GetPortfolioByEmployee()
            .subscribe(
              (data) => {
                // alert(JSON.stringify(data));
                this._PortfolioList = data as PortfolioDTO[];
                this._ExistingPortfolioOfProjects = this._PortfolioList;
                console.log("Portfolio:", this._ExistingPortfolioOfProjects);
                this.cdr.detectChanges();
              });
        });
      this.notifyService.showInfo("Successfully" + ' ' + 'Updated' + ' ' + LengthOfSelectedItems + ' ' + 'Project', ' New Portfolio Created');
    }
    this.PortfolioList = false;
    this._ProjectList = true;
    this.Portfolio_FormGroup.reset();
    this.master_CheckBox();
    this.btn_CreatePortfolio = true;
    this.Selected_project_List = [];
    this.selectedItemsList = [];
    this.Obj_Portfolio_DTO.Status = null;
  }
  //Company DropDown Bind
  GetCompanies() {
    this.service.GetCompanies().subscribe(res => {
      this.Company_List = res as CompanyDTO[];
      // console.log(this.Company_List)
      this.dropdownSettings_Cmp = {
        singleSelection: true,
        idField: 'Com_No',
        textField: 'Com_Name',
        itemsShowLimit: 1,
        allowSearchFilter: true
      };
    })
  }
  //Employee DropDown Bind
  OnEmpSelect(emp: string) {
    let arr=[];
    this.EmployeeDropdown = emp['Emp_No'];
    console.log("Selected Employees---->",JSON.stringify(this.ngEmployeeDropdown));
    this.ngEmployeeDropdown2=this.ngEmployeeDropdown;
    this.ngEmployeeDropdown2.forEach(element => {
      arr.push({ Emp_No:element.Emp_No})
      this._SelectedEmployees = arr;
    });
    console.log("Final Result",this._SelectedEmployees)
    console.log("Only EmpNo----->",this.EmployeeDropdown)
    this._ErrorMessage_User = "";
  }
  OnEmpDeselect() {
    let arr=[];
    this.ngEmployeeDropdown2=this.ngEmployeeDropdown;
    this.ngEmployeeDropdown2.forEach(element => {
      arr.push({ Emp_No:element.Emp_No})
      this._SelectedEmployees = arr;
    });
    console.log("Final Result",this._SelectedEmployees)
    //this.ngEmployeeDropdown = [{}];
  }
  OnCompanySelect(CompNo: string) {
    debugger
    this._ErrorMessage_comp = "";
    this._CompanyNo = CompNo['Com_No'];
    this.CompanyDropdown = this._CompanyNo;
    // alert(this.CompanyDropdown);
    let PortfolioId: any = sessionStorage.getItem('Pid');
    this.service.GetEmployeesby_CompNo(this._CompanyNo, PortfolioId)
      .subscribe((data) => {
        this.EmployeeList = data as EmployeeDTO[];
        //console.log("employee lst",this.EmployeeList);
        this.dropdownSettings_Emp = {
          singleSelection: true,
          idField: 'Emp_No',
          textField: 'TM_DisplayName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          itemsShowLimit: 5,
          allowSearchFilter: true
        };
      });
  }
  OnCmpDeselect() {
    this.ngCompanyDropdown = [{}];

  }
  funforGetComp_Users() {
    this.GetCompanies();
  }
  GetUserDetails() {
    this.service.NewGetUserDetails(this.Current_user);
  }

  GetPortfolioByEmployee() {
    debugger
    this.service.GetPortfolioByEmployee()
      .subscribe(
        (data) => {
          // alert(JSON.stringify(data));
          this._PortfolioList = data as PortfolioDTO[];
          this._ExistingPortfolioOfProjects = this._PortfolioList;
          console.log("Portfolio:", this._ExistingPortfolioOfProjects);
          this.cdr.detectChanges();
        });
  }

  _PortfolioListTable_Bttn() {
    this._ShareDetailsList = [];
    this._ProjectsListBy_Pid = [];
    this._PortfolioListTable = true;
    this.PortfolioList = false;
    this._PreferencesList = [];
  }
  Radio_View_fullaccess(val: string) {
    this._ErrorMessage_Pref = "";
    this._Preferences = val;
  }
  share() {
    debugger
    // debugger
    if (this.CompanyDropdown == undefined) {
      return this._ErrorMessage_comp = "* Please Select Company";
    }
    if (this.EmployeeDropdown == undefined) {
      return this._ErrorMessage_User = "* Please Select User Name";
    }
    if (this.preferences == null) {
      return this._ErrorMessage_Pref = "* Please Select Preferences";
    }
    if (this.Current_user_ID == this.EmployeeDropdown.replace(/\s/g, "")) {
      this.notifyService.showInfo("You Can't Share Portfolio By YourSelf", "Sorry");
      //this.Close_ShareModel();
    }

    else {
      debugger
      this.ObjSharePortfolio.CompanyId = this._CompanyNo;
      this.ObjSharePortfolio.EmployeeId = this.EmployeeDropdown;
      this.ObjSharePortfolio.Portfolio_ID = this._Pid;
      this.ObjSharePortfolio.Preference = this._Preferences;
      this.ObjSharePortfolio.Shared_By = this.Current_user_ID;
      this.ObjSharePortfolio.IsActive = true;
      this.service.SharePortfolio(this.ObjSharePortfolio);
      this.notifyService.showInfo("Successfully", "Portfolio Shared");
      this.Close_ShareModel();
      this._PortfolioListTable = true;
      this.PortfolioList = false;
      this.cdr.detectChanges();
    }
  }
  Close_ShareModel() {
    this.CompanyDropdown = "";
    this.EmployeeDropdown = "";
    this._ErrorMessage_comp = "";
    this._ErrorMessage_User = "";
    this._ErrorMessage_Pref = "";
    this.preferences = null;
    this.ngEmployeeDropdown.size==0;
  }
  GetDateDiff() {
    var date2 = new Date("06/30/2019");
    var date1 = new Date("07/30/2019");
    if (date1 > date2) {
      alert("Greater Than Date2 ");
    }
    else {
      alert("less than Date 1")
    }
  }
}































