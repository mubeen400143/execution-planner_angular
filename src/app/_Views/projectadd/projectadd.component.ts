import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProjectTypeService } from "src/app/_Services/project-type.service";
import { ProjecttypeDTO } from 'src/app/_Models/projecttype-dto';
@Component({
  selector: 'app-projectadd',
  templateUrl: './projectadd.component.html',
  styleUrls: ['./projectadd.component.css']
})
export class ProjectaddComponent implements OnInit {
  myform: FormGroup;
  selectedId: number;
  _ProjectTypeList: ProjecttypeDTO[];
  _ProjectList: ProjecttypeDTO[];


  hidden: boolean;
  findedData: any;
  _arryList = [];
  constructor(private formBuilder: FormBuilder, public service: ProjectTypeService) {
  }

  ngOnInit() {
   // this.getProjectTypeList();
    this.hidden = true;
    console.log()
    this.myform = this.formBuilder.group({
      projectType: ['', Validators.required],
    });
  }
  searchText;
  ProjList = [
    {
      ProjectTypeId: 1,
      ProjectType: "Core Type",
      children: [
        { project_id: 101, project_Name: "Proj-One CoreType", status: "OnHold" },
        { project_id: 102, project_Name: "Proj-Two CoreType", status: "InProcess" },
        { project_id: 103, project_Name: "Proj-Three CoreType", status: "Completed" },
      ]
    },
    {
      ProjectTypeId: 2,
      ProjectType: "Secondary Type",
      children: [
        { project_id: 201, project_Name: "Proj-One Secondary", status: "Completed" },
        { project_id: 202, project_Name: "Proj-Two Secondary", status: "OnHold" },
        { project_id: 203, project_Name: "Proj-Three Secondary", status: "Rejected" },
      ]
    },
    {
      ProjectTypeId: 3,
      ProjectType: "Standard Type",
      children: [
        { project_id: 301, project_Name: "Proj-One Standard", status: "Rejected" },
        { project_id: 302, project_Name: "Proj-Two Standard", status: "InProcess" },
        { project_id: 303, project_Name: "Proj-Three Standard", status: "Completed" },
      ]
    },
  ];

  onSubmit() {
  }
  OnProjectTypeChange(projTypeId) {
    this._arryList = [];
    this.selectedId = projTypeId;

    this.findedData = this.ProjList.find(i => i.ProjectTypeId === this.selectedId);
    this._arryList.push(this.findedData);
    if (typeof this._arryList === 'undefined') {
      return null;
    }
    console.log("Finadata", this._arryList);
    this.hidden = false;
    // this.ProjList.filter(s => {
    //   return s.ProjectTypeId = this.selectedId
    // });
  }
  // getProjectTypeList() {
  //   this.service.GetProjectTypeList().
  //     subscribe(res =>
  //       this._ProjectTypeList = res as ProjecttypeDTO[]);
  //   console.log("APi List", JSON.stringify(this._ProjectTypeList))
  // }

  OnProjectTypeChangeAPI(projTypeId) {
    debugger
    this.selectedId = projTypeId;
    this.GetProjects(projTypeId)
  }

  GetProjects(Pid) {
    debugger
    this.service.GetProjects(Pid)
      .subscribe((data) => this._ProjectList = data as ProjecttypeDTO[]);
      console.log("Project List :",this._ProjectList);
  }
}
