import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginDTO } from 'src/app/_Models/login-dto';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';

import { Login } from 'src/app/_Interface/login';
import { AuthService } from 'src/app/_Services/auth.service'
import { ProjectTypeService } from 'src/app/_Services/project-type.service';
import { HomeComponent } from '../home/home.component';
import { ChangeDetectorRef } from '@angular/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  _LoginForm: FormGroup;
  submitted = false;
  Obj_ILoginDTO: Login;
  DB_username: string;
  DB_password: string;
  UserDetails_List: UserDetailsDTO[];
  isLoading = false;
  InValidPassword=false;
  InValidUserName=false;
  private ObjHomeComp: HomeComponent
  EmpNo: string; EmpCompNo: string; SystemRole: string;
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private cd: ChangeDetectorRef,
    private authService: AuthService,
    private service: ProjectTypeService) {
    this.Obj_ILoginDTO = new LoginDTO;


  }
  //Login Form Variables
  // model: Login = { UserName: '', Password: '' }

  loginForm: FormGroup;
  message: string;
  returnUrl: string;
  //---end---
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userid: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = '/Dashboard';
    this.authService.logout();
  }
  get f() { return this.loginForm.controls; }

  login() {
    debugger
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    else {

      this.Sendlogin_Credentials();
      this.isLoading = true;
      setTimeout(() => {
        this.isLoading = false;
      }, 3000)
    }
  }
  Sendlogin_Credentials() {
   
    this.Obj_ILoginDTO.UserName = this.f.userid.value;
    this.Obj_ILoginDTO.Password = this.f.password.value;
    this.service.LoginCredentials(this.Obj_ILoginDTO)
      .subscribe(
        (data) => {
          this.UserDetails_List = data as UserDetailsDTO[];
          this.DB_username = this.UserDetails_List[0]['UserName'].replace(/\s/g, "").toLowerCase();
          this.DB_password = this.UserDetails_List[0]['Password'];

          this.EmpNo = data[0]['Emp_No'].replace(/\s/g, "");
          this.EmpCompNo = data[0]['Emp_Comp_No'].replace(/\s/g, "");
          this.SystemRole = data[0]['Emp_SystemRole'];

          sessionStorage.setItem('EmpNo', this.EmpNo);
          sessionStorage.setItem('EmpCompNo', this.EmpCompNo);
          sessionStorage.setItem('SystemRole', this.SystemRole);

          this.match_Credentials();
        });
  }
  match_Credentials() {
    debugger
    // if(this.f.userid.value.toLowerCase()!= this.DB_username){
    //   this.InValidUserName=true;
    // }
    // if(this.f.userid.value.toLowerCase()!=this.DB_username){
    //   this.InValidPassword=true;
    // }
    if (this.f.userid.value.toLowerCase() == this.DB_username && this.f.password.value == this.DB_password) {
      console.log("Login successful");
      localStorage.setItem('isLoggedIn', "true");
      this.InValidPassword=false;
      this.cd.detectChanges();
      localStorage.setItem('_Currentuser', this.DB_username);
      this.router.navigate([this.returnUrl]);

    }
    else {
      console.log("Invalid Login");
      this.authService.logout();
      this.InValidPassword=true;
      this.cd.detectChanges();
    //  this.message = "Please check your userid and password";
    }
  }
}


















//---------------------------------Commented Code-----------------------------------------//
// .subscribe(data =>[{ 
//   a:data[0]['UserName']},{b:data[0]['UserName']}]);
// console.log("DBUsername===========>",a,b)

 //let value = Object.values(this.User_Details);
      // if (this.User_Details) {
      //   let value = Object.values(this.User_Details);
      //   this.DB_username = value[0]['UserName'].replace(/\s/g, "");
      //   this.DB_password = value[0]['Password'];
      // }
      // else {
      //   return null;
      // }