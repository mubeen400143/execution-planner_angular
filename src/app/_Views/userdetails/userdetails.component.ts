import { Component, OnInit } from '@angular/core';

import { LoginComponent } from '../login/login.component';
import { UserDetailsDTO } from 'src/app/_Models/user-details-dto';
import { UserDetailsService } from 'src/app/_Services/user-details.service';
import { Login } from 'src/app/_Interface/login';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_Services/auth.service';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.css']
})
export class UserdetailsComponent implements OnInit {

  UserName: string;
  empId: string;
  User_List: UserDetailsDTO[];
  objLoginDTO: Login;
  model: Login;
 
  constructor(private service: UserDetailsService,
    private router: Router,
    private authService: AuthService) {
  }
  ngOnInit() {
    this.UserName = localStorage.getItem('token');
    //this.GetUserDetails();
  }
  logout() {
    console.log('logout');
    this.authService.logout();
    console.log(this.authService.logout());
    this.router.navigate(['/login']);
  }
  GetUserDetails() {
    debugger
    this.service.NewGetUserDetails(this.UserName)
      .subscribe(
        (data) => {
          this.User_List = data as UserDetailsDTO[];
          console.log("=====", this.User_List);
        });
  }
}
