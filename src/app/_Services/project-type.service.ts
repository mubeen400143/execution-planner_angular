import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ProjecttypeDTO } from '../_Models/projecttype-dto';
import { ApiurlService } from './apiurl.service';
import { ProjectDetailsDTO } from '../_Models/project-details-dto';
import { EmployeeDTO } from '../_Models/employee-dto';
import { PortfolioDTO } from 'src/app/_Models/portfolio-dto';
import { CompanyDTO } from 'src/app/_Models/company-dto';
import { LoginDTO } from '../_Models/login-dto';
import { Login } from 'src/app/_Interface/login';
import { UserDetailsDTO } from '../_Models/user-details-dto';
import { HomeComponent } from '../_Views/home/home.component';
import { Shareportfolio_DTO } from '../_Models/shareportfolio';
import { StatusDTO } from '../_Models/status-dto';
@Injectable({
  providedIn: 'root'
})
export class ProjectTypeService {
  ProjectDetails_List: ProjectDetailsDTO[];
  ProjectTypeList: ProjecttypeDTO[];
  ObjprojectTypeDto: ProjecttypeDTO;
  ObjDto: ProjectDetailsDTO;
  EmployeeList: EmployeeDTO[];
  ObjEmployeeDetails: EmployeeDTO;
  objPortfolioDTO: PortfolioDTO;
  obj_CompanyDTO: CompanyDTO;
  Obj_LoginDTO: LoginDTO;
  User_Details: LoginDTO[];
  ObjUserDetails: UserDetailsDTO;
  Obj_ILogin: Login[];
  user_DetailList: UserDetailsDTO[];
  Teamlist: ProjectDetailsDTO[];
  StatusList: ProjectDetailsDTO[];
  userDTO: UserDetailsDTO;
  EmpNo: string; EmpCompNo: string; SystemRole: string;
  ObjHomeComponent: HomeComponent;
  Portfolio_List: PortfolioDTO[];
  Project_TypeList = [];
  _ObjSharePortfolio: Shareportfolio_DTO;
  ObjStatusDTO: StatusDTO;

  constructor(private http: HttpClient, private commonUrl: ApiurlService) {
    this.ObjprojectTypeDto = new ProjecttypeDTO;
    this.ObjEmployeeDetails = new EmployeeDTO;
    this.Obj_LoginDTO = new LoginDTO;
    this.userDTO = new UserDetailsDTO;
    this.ObjUserDetails = new UserDetailsDTO;
    this._ObjSharePortfolio = new Shareportfolio_DTO;
    this.objPortfolioDTO = new PortfolioDTO;
    this.ObjStatusDTO = new StatusDTO;
  }
  readonly rootUrl = this.commonUrl.apiurl;

  GetProjectTypeList() {
    let EmpNo = sessionStorage.getItem('EmpNo');
    this.ObjUserDetails.Emp_No = EmpNo;
    return this.http.post(this.rootUrl + "TestAPI/NewGetProjectTypes", this.ObjUserDetails)
    // .subscribe(data => {this.ProjectTypeList = data as ProjecttypeDTO[]
    //   this.Project_TypeList=this.ProjectTypeList.sort((a, b) => (a.Exec_BlockName > b.Exec_BlockName) ? 1 : -1);
    // });
  }
  GetProjects(Ptype_Id) {
    this.ObjprojectTypeDto.ProjectType_ID = Ptype_Id;
    return this.http.post(this.rootUrl + "TestAPI/NewGetProjects", this.ObjprojectTypeDto)
  }
  GetProjects_EPDB() {
    return this.http.post(this.rootUrl + "TestAPI/NewGetProjectsDetails", this.ObjDto)
    // .subscribe((data) =>this.ProjectDetails_List = data as ProjectDetailsDTO[]);
  }

  GetProjectsBy_portfolioId(pid: number) {

    // let P_Id: string = sessionStorage.getItem('Pid');
    // let parsePid: number = +P_Id;
    this.objPortfolioDTO.Portfolio_ID = pid;
    this.objPortfolioDTO.EmployeeId = sessionStorage.getItem('EmpNo');
    return this.http.post(this.rootUrl + "TestAPI/NewGetPortfolioSharePreferences_Json", this.objPortfolioDTO)
  }
  GetProjectsByUserName(count, Ptype, team, stat, txtsrch) {
    debugger
    let EmpNo = sessionStorage.getItem('EmpNo');
    let Emp_Comp_No = sessionStorage.getItem('EmpCompNo');
    let Systemrole = sessionStorage.getItem('SystemRole');
    if (team==undefined) {
      this.ObjUserDetails.Emp_No = EmpNo;
    }
    else {
      this.ObjUserDetails.Emp_No = team;
    }
    this.ObjUserDetails.Emp_Comp_No = Emp_Comp_No;
    this.ObjUserDetails.SystemRole = Systemrole;
    //this.ObjUserDetails.Project_Code = '';
    this.ObjUserDetails.Exec_BlockName = Ptype;
    this.ObjUserDetails.Status = stat;
    this.ObjUserDetails.PageNumber = count;
    this.ObjUserDetails.SearchText = txtsrch;
    return this.http.post(this.rootUrl + "TestAPI/NewGetProjectDetailsByUserName", this.ObjUserDetails);
    // .subscribe(
    //   (data) => {
    //     this.ProjectDetails_List = data as ProjectDetailsDTO[];
    // console.log("before Removal--->", this.ProjectDetails_List);
    //this.Teamlist = this.ProjectDetails_List.filter((el, i, a) => i === a.indexOf(el))


    // this.Teamlist = Array.from(this.ProjectDetails_List.reduce((m, t) => m.set(t.TM_DisplayName, t), new Map()).values());
    // this.StatusList = Array.from(this.ProjectDetails_List.reduce((m, t) => m.set(t.Status, t), new Map()).values());
    // this.Teamlist = this.Teamlist.sort((a, b) => (a.TM_DisplayName > b.TM_DisplayName) ? 1 : -1);
    // this.StatusList = this.StatusList.sort((a, b) => (a.Status > b.Status) ? 1 : -1);
    //});
  }
  GetEmployeesById(EmpNo: string) {
    this.ObjEmployeeDetails.Emp_No = EmpNo;
    return this.http.post(this.rootUrl + "TestAPI/NewGetEmployeById", this.ObjEmployeeDetails)
    // .subscribe((data) =>this.ProjectDetails_List = data as ProjectDetailsDTO[]);
  }
  // GetEmployees() {
  //   return this.http.post(this.rootUrl + "TestAPI/NewGetEmployees", this.ObjEmployeeDetails)
  //     .subscribe((data) => this.EmployeeList = data as EmployeeDTO[]);
  // }
  GetEmployeesby_CompNo(cmpNo, Pid) {
    this.ObjEmployeeDetails.Emp_Comp_No = cmpNo;
    this.ObjEmployeeDetails.Portfolio_ID = Pid;
    return this.http.post(this.rootUrl + "TestAPI/NewGetEmployeesByComp_No", this.ObjEmployeeDetails);
  }
  SavePortfolio(objFromComp) {
    debugger
    this.objPortfolioDTO = objFromComp;
    this.objPortfolioDTO.Portfolio_ID = objFromComp.Portfolio_ID;
    this.objPortfolioDTO.Portfolio_Name = objFromComp.Portfolio_Name;
    this.objPortfolioDTO.Status;
    this.objPortfolioDTO.SelectedProjects = objFromComp.SelectedProjects;
    if (this.objPortfolioDTO.Portfolio_ID == 0) {
      this.objPortfolioDTO.Created_By = this.ObjUserDetails.Emp_No;
      this.objPortfolioDTO.Modified_By = null;
    }
    else {
      this.objPortfolioDTO.Created_By = null;
      this.objPortfolioDTO.Modified_By = this.ObjUserDetails.Emp_No;
    }
    return this.http.post(this.rootUrl + "/TestAPI/NewInsertPortfolio", objFromComp);
      // .subscribe(data => {
      //   this.objPortfolioDTO = data as PortfolioDTO;
      //   // this.getDocTypeData();
      // });
    
  }
  //Get Portfolio
  GetPortfolio() {
    return this.http.post(this.rootUrl + "TestAPI/NewGetPortfolio", this.objPortfolioDTO)
  }
  //Get Companies
  GetCompanies() {
    return this.http.post(this.rootUrl + "TestAPI/NewGetCompanies", this.obj_CompanyDTO)
  }
  LoginCredentials(objLoginDetails) {
    //
    return this.http.post(this.rootUrl + "TestAPI/NewGetLoginDetails", objLoginDetails);
    // .subscribe(data => {
    //   this.User_Details = data as LoginDTO[];
    //});
  }
  NewGetUserDetails(UserName) {
    this.userDTO.UserName = UserName;
    return this.http.post(this.rootUrl + "TestAPI/NewGetUserDetails", this.userDTO).
      subscribe(
        (data) => {
          this.user_DetailList = data as UserDetailsDTO[]
          this.EmpNo = data[0]['Emp_No'].replace(/\s/g, "");
          this.EmpCompNo = data[0]['Emp_Comp_No'].replace(/\s/g, "");
          this.SystemRole = data[0]['Emp_SystemRole'];
        });
  }
  GetPortfolioByEmployee() {
    debugger
    let EmpNo = sessionStorage.getItem('EmpNo');
    this.ObjUserDetails.Emp_No = EmpNo;
    return this.http.post(this.rootUrl + "TestAPI/NewGetPortfolioByEmployee", this.ObjUserDetails)
    // .subscribe(
    //   (data) => {
    //     this.Portfolio_List = data as PortfolioDTO[];
    //     console.log("PortfolioList------>", this.Portfolio_List);
    //   });
  }
  SharePortfolio(_ObjShare: Shareportfolio_DTO) {
    return this.http.post(this.rootUrl + "TestAPI/NewInsertUpdateSharePortfolio", _ObjShare)
      .subscribe(data => {
        this._ObjSharePortfolio = data as Shareportfolio_DTO;
      });
  }

  GetPreferences(empId, pid) {
    debugger
    this._ObjSharePortfolio.EmployeeId = empId;
    this._ObjSharePortfolio.Portfolio_ID = pid;
    return this.http.post(this.rootUrl + "TestAPI/NewGetPreferencesByEmployeeId", this._ObjSharePortfolio)
  }

  GetShareDetailsBy_PId(pid) {
    this._ObjSharePortfolio.Portfolio_ID = pid;
    return this.http.post(this.rootUrl + "TestAPI/NewGetShareDetailsByPortfolio_ID", this._ObjSharePortfolio)

  }

  GetStatusByPortfolioId(pid) {
    this.ObjStatusDTO.Portfolio_ID = pid;
    return this.http.post(this.rootUrl + "TestAPI/NewGetStatus", this._ObjSharePortfolio)
  }
}
