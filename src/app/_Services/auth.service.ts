import { Injectable } from '@angular/core';
import { Login} from 'src/app/_Interface/login';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  logout() :void {    
    localStorage.setItem('isLoggedIn','false');    
    localStorage.removeItem('token');    
    }    
}
