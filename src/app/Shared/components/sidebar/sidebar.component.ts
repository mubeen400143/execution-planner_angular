import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_Services/auth.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  _CurrentUser: string;
  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this._CurrentUser = localStorage.getItem('_Currentuser');
  }
  logout(){
    debugger
    console.log('logout');
    this.authService.logout();
    //localStorage.clear();
    // console.log(this.authService.logout()); 
    this.router.navigate(['/login']);
    window.sessionStorage.clear();
    //  window.localStorage.clear();
  }
}
