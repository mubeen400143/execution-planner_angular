import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './_Layouts/default/default.component';
import { HomeComponent } from './_Views/home/home.component';
import { PostComponent } from './_Views/post/post.component';
import { LoginComponent } from './_Views/login/login.component';
import { UserdetailsComponent } from './_Views/userdetails/userdetails.component';
import { ProjectaddComponent } from './_Views/projectadd/projectadd.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: '', component: LoginComponent },
  {path:'Home',component:DefaultComponent},
  {
    path: '',
    component: DefaultComponent,
    children: [{
      path: 'Dashboard',
      component: HomeComponent,canActivate:[AuthGuard]
    }, {
      path: 'posts',
      component: PostComponent
    },
    {
      path: 'profile',
      component: UserdetailsComponent
    },
  {
    path:'NewProject',
    component:ProjectaddComponent
  }]
  },
  { path: 'login', component: LoginComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
