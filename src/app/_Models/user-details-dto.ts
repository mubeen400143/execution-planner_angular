export class UserDetailsDTO {
    UserName: string;
    Password: string;
    Active: boolean;
    Emp_No: string;
    Emp_Comp_No: string;
    SystemRole: string;
    Created_On: Date;
    Expired_On: Date;
    Loggedon: Date;
    Sales_Acc: string;
    Rejoin: boolean;
    DB_UserId: string;
    DB_Password: string;
    SearchText: string;
    DateOfJoin: Date;
    Emp_Email: string;
    Emp_First_Name: string;
    Emp_Last_Name: string;
    Emp_Second_Name: string;
    Gender: string;
    Emp_Dept_No: string;

    ProjectType: string;
    Status: string;
    ProjectCodes: string;
    Project_Code: string;
    Exec_BlockName: string;

    PageNumber: number;
    RowsOfPage: number;
}
